import sys


def solve(file):
    sol1 = 0
    sol2 = 0
    with open(file, 'r') as f:
        captcha = f.read()
        for i, c in enumerate(captcha):
            if captcha[i-1] == c:
                sol1 += int(c)
            j = int((i + len(captcha)/2) % len(captcha))
            if captcha[j] == c:
                sol2 += int(c)
        print('Part 1 Solution: %d' % sol1)
        print('Part 2 Solution: %d' % sol2)


print('Advent of Code 2017 - Day 1')
file = sys.argv[1]
solve(file)
