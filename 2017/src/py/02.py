import sys


def solve(file):
    sol1 = 0
    sol2 = 0
    with open(file, 'r') as f:
        for line in f:
            nums = [int(num) for num in line.split()]
            sol1 += max(nums) - min(nums)
            for i in range(0, len(nums)-1):
                for j in range(i+1, len(nums)):
                    if not nums[i] % nums[j]:
                        sol2 += nums[i] / nums[j]
                    elif not nums[j] % nums[i]:
                        sol2 += nums[j] / nums[i]
    print('Part 1: %d' % sol1)
    print('Part 2: %d' % sol2)


print('Advent of Code - Day 2')
file = sys.argv[1]
solve(file)
