import sys


def spiral1(size=1200):
    val = 1
    curr = (0,0)
    spiral = {(0,0):1}
    # walk the spiral
    for i in range(0, size):
        for j in range(0, i//2+1):
            if i%4 == 0:
                curr = (curr[0]+1, curr[1])
            elif i%4 == 1:
                curr = (curr[0], curr[1]+1)
            elif i%4 == 2:
                curr = (curr[0]-1, curr[1])
            elif i%4 == 3:
                curr = (curr[0], curr[1]-1)
            val += 1
            spiral[curr] = val
    return spiral


def neighbors(pos):
    nn = (pos[0]+0, pos[1]+1)
    ne = (pos[0]+1, pos[1]+1)
    ee = (pos[0]+1, pos[1]+0)
    se = (pos[0]+1, pos[1]-1)
    ss = (pos[0]+0, pos[1]-1)
    sw = (pos[0]-1, pos[1]-1)
    ww = (pos[0]-1, pos[1]+0)
    nw = (pos[0]-1, pos[1]+1)
    return [nn, ne, ee, se, ss, sw, ww, nw]


def spiral2(size=20):
    curr = (0,0)
    spiral = {(0,0):1}
    for i in range(0, size):
        for j in range(0, i//2+1):
            if i%4 == 0:
                curr = (curr[0]+1, curr[1])
            elif i%4 == 1:
                curr = (curr[0], curr[1]+1)
            elif i%4 == 2:
                curr = (curr[0]-1, curr[1])
            elif i%4 == 3:
                curr = (curr[0], curr[1]-1)
            val = sum(spiral.get(n, 0) for n in neighbors(curr))
            spiral[curr] = val
    return spiral


def solve(file):
    with open(file, 'r') as f:
        input_num = int(f.read())
    spiral_1 = spiral1()
    for k,v in spiral_1.items():
        if v == input_num:
            print('Part 1 Solution: %d' % (abs(k[0]) + abs(k[1])))
            break
    spiral_2 = spiral2()
    sorted_vals = sorted(spiral_2.values())
    for i in range(0, len(sorted_vals)):
        if sorted_vals[i] > input_num:
            print('Part 2 Solution: %d' % sorted_vals[i])
            break


print('Advent of Code 2017 - Day 3')
solve(sys.argv[1])
