import sys


def decompose(word):
    letter_count = dict()
    for c in word:
        if c not in letter_count:
            letter_count[c] = 1
        else:
            letter_count[c] += 1
    return letter_count


def solve(file):
    sol1 = 0
    with open(file, 'r') as f:
        for line in f:
            seen = []
            valid = 1
            phrase = line.split()
            for word in phrase:
                d_word = decompose(word)
                if d_word in seen:
                    valid = 0
                    break
                else:
                    seen.append(d_word)
            sol1 += valid
    print('Part 1 Solution: %d' % sol1)


print('Advent of Code 2017 - Day 4')
solve(sys.argv[1])
