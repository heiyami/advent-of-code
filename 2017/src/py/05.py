import sys


def solve(file):
    maze1 = []
    maze2 = []
    with open(file, 'r') as f:
        for offset in f:
            maze1.append(int(offset))
            maze2.append(int(offset))
    curr1 = 0; steps1 = 0
    curr2 = 0; steps2 = 0
    # Part 1
    while curr1 < len(maze1):
        maze1[curr1] += 1
        curr1 += maze1[curr1] - 1
        steps1 += 1
    # Part 2
    while curr2 < len(maze2):
        diff = -1 if maze2[curr2] > 2 else 1
        maze2[curr2] += diff
        curr2 += maze2[curr2] - diff
        steps2 += 1

    print('Part 1 Solution: %d' % steps1)
    print('Part 2 Solution: %d' % steps2)


print('Advent of Code 2017 - Day 5')
solve(sys.argv[1])
