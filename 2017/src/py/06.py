import sys


def realloc(mem):
    max_value = max(mem)
    max_index = mem.index(max_value)
    loops = max_value//len(mem)
    carry_over = max_value % len(mem)
    extra = [x % len(mem) for x in range(max_index+1, max_index+carry_over+1)]
    mem[max_index] = 0
    for i in range(0, len(mem)):
        mem[i] += loops+1 if i in extra else loops


def solve(file):
    seen = []
    with open(file, 'r') as f:
        mem = [int(bank) for bank in f.read().split()]
    while tuple(mem[:]) not in seen:
        seen.append(tuple(mem[:]))
        realloc(mem)
    print('Part 1 Solution: %d' % len(seen))
    print('Part 2 Solution: %d' % (len(seen) - seen.index(tuple(mem))))


print('Advent of Code 2017 - Day 6')
solve(sys.argv[1])
