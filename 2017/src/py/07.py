import sys
import queue
from collections import Counter


def parse(file):
    with open(file, 'r') as f:
        p_input = [line.split() for line in f]
    tree = {}
    for elem in p_input:
        n = elem[0]
        w = int(elem[1][1:-1])
        c = [x.replace(',', '') for x in elem[3:]]
        tree[n] = [w, c]
    return tree


def depth(tree, node):
    d, cd = 1, 0
    for c in tree[node][1]:
        cd = max(cd, depth(tree, c))
    return d+cd


def get_root(tree):
    max_depth = 0
    for node in iter(tree):
        if depth(tree, node) > max_depth:
            max_depth = depth(tree, node)
            root = node
    return root


def t_weight(tree, node):
    t_w = tree[node][0]
    t_w += sum(t_weight(tree, c) for c in tree[node][1])
    return t_w


def walk(tree, start):
    """ DFS """
    # does this really work?
    visited = []
    stack = [start]
    while stack:
        curr = stack.pop()
        if curr in visited:
            continue
        visited.append(curr)
        for child in tree[curr][1]:
            stack.append(child)
    yield from list(reversed(visited))


def solve(file):
    tree = parse(file)
    root = get_root(tree)
    print('Part 1 Solution: %s' % root)
    for node in walk(tree, root):
        if not tree[node][1]:
            continue
        ct_weights = []
        for c in tree[node][1]:
            ct_weights.append(t_weight(tree, c))
        if ct_weights.count(ct_weights[0]) != len(ct_weights):
            # one of the children is unbalanced
            ctr = Counter(ct_weights)
            right = ctr.most_common(1)[0][0]
            wrong = list(reversed(ctr.most_common()))[0][0]
            diff = right - wrong
            for c in tree[node][1]:
                if t_weight(tree, c) == wrong:
                    print('Part 2 Solution: %d' % (tree[c][0] + diff))
                    return


file = sys.argv[1]
solve(file)
