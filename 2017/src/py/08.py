import sys


def parse(file):
    # regval[reg] = [curr, max]
    regval = {}
    with open(file, 'r') as f:
        for line in f:
            regA, op, valA, _, regB, comp, valB = line.split()
            op = '+=' if 'inc' in op else '-='
            if regA not in regval:
                regval[regA] = [0] * 2
            if regB not in regval:
                regval[regB] = [0] * 2
            # update current value
            regA = 'regval[\'{}\'][0]'.format(regA)
            regB = 'regval[\'{}\'][0]'.format(regB)
            cond = 'if ' + ' '.join([regB, comp, valB]) + ' else 0'
            inst = '{} {} {} {}'.format(regA, op, valA, cond)
            exec(inst)
            # update max value
            inst = '{regA}[1] = {regA}[0] \
                if {regA}[0] > {regA}[1] else {regA}[1]'.format(regA=regA[:-3])
            exec(inst)
    return regval


def solve(file):
    regval = parse(file)
    print('Part 1 Solution: %d' % max(v[0] for v in regval.values()))
    print('Part 2 Solution: %d' % max(v[1] for v in regval.values()))


print('Advent of Code 2017 - Day 8')
solve(sys.argv[1])
