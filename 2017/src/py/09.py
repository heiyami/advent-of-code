import sys


def solve(file):
    with open(file, 'r') as f:
        scores = []
        depth, mode, garb_count = 0, 0, 0
        for line in f:
            for c in line:
                if mode:
                    if mode == 2:
                        mode = 1
                        continue
                    elif c == '!':
                        mode = 2
                    elif c == '>':
                        mode = 0
                    else:
                        garb_count += 1
                else:
                    if c == '<':
                        mode = 1
                    elif c == '{':
                        depth += 1
                    elif c == '}':
                        scores.append(depth)
                        depth -= 1
    print('Part 1 Solution: %d' % sum(scores))
    print('Part 2 Solution: %d' % garb_count)


print('Advent of Code 2017 - Day 9')
print(solve(sys.argv[1]))
