import sys
from functools import reduce
from operator import ixor


def swap(lst, a, b):
    tmp = lst[a]
    lst[a] = lst[b]
    lst[b] = tmp


def rev(lst, start, length):
    for i in range(0, length//2):
        swap(lst, (start+i)%len(lst), (start+length-i-1)%len(lst))


def k_hash(lengths, nums=[i for i in range(0,256)], pos=0, skip=0):
    for l in lengths:
        if l > 256:
            continue
        rev(nums, pos, l)
        pos += l + skip
        skip += 1
    return (nums, pos, skip)


def s_hash(lengths):
    s_hash, _pos, _skip = [i for i in range(0,256)], 0, 0
    for i in range(0,64):
        s_hash, _pos, _skip = k_hash(lengths, nums=s_hash ,pos=_pos, skip=_skip)
    return s_hash


# this is fine
def d_hash(s_hash):
    d_hash = ''
    for i in range(0,16):
        hexit = hex(reduce(ixor, s_hash[16*i+0:16*i+16]))[2:]
        d_hash += '0' + hexit if len(hexit) == 1 else hexit
    return d_hash


def solve1(file):
    with open(file, 'r') as f:
        lengths = [int(l) for l in f.read().split(',')]
    knot_hash = k_hash(lengths)[0]
    print('Part 1 Solution: %d' % (knot_hash[0] * knot_hash[1]))


def solve2(file):
    with open(file, 'r') as f:
        data = f.read().strip()
    byte_seq = [ord(x) for x in data]
    lengths = byte_seq + [17, 31, 73, 47, 23]
    sparse_hash = s_hash(lengths)
    dense_hash = d_hash(sparse_hash)
    print('Part 2 Solution: %s' % dense_hash)


print('Advent of Code 2017 - Day 10')
solve1(sys.argv[1])
solve2(sys.argv[1])
