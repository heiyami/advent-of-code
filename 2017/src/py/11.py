import sys


def distance(coords):
    return (abs(coords[0]) + abs(coords[1]) + abs(coords[2]))/2


def walk(curr, step):
    step = step.strip()
    if step == 'n':
        curr[1] += 1
        curr[2] -= 1
    elif step == 'ne':
        curr[0] += 1
        curr[2] -= 1
    elif step == 'nw':
        curr[1] += 1
        curr[0] -= 1
    elif step == 's':
        curr[2] += 1
        curr[1] -= 1
    elif step == 'se':
        curr[0] += 1
        curr[1] -= 1
    elif step == 'sw':
        curr[2] += 1
        curr[0] -= 1
    else:
        raise ValueError('Not a valid step: %s' % step)


def parse(file):
    with open(file, 'r') as f:
        path = f.read().split(',')
    return path


def solve(file):
    path = parse(file)
    curr = [0,0,0]
    farthest = 0
    for i in range(0,len(path)):
        walk(curr, path[i])
        farthest = distance(curr) if distance(curr) > farthest else farthest

    print('Part 1 Solution: %d' % distance(curr))
    print('Part 2 Solution: %d' % farthest)


print('Advent of Code 2017 - Day 11')
solve(sys.argv[1])
