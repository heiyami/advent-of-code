import sys


def parse(file):
    graph = {}
    with open(file, 'r') as f:
        for line in f:
            line = line.replace(',', '').strip().split()
            p = int(line[0])
            adj = {int(x) for x in line[2:]}
            graph[p] = adj
    return graph


def group(graph, vertex):
    seen = set()
    todo = [vertex]
    while todo:
        v = todo.pop()
        if v not in seen:
            seen.add(v)
            todo.extend(graph[v] - seen)
    return seen


def solve(file):
    graph = parse(file)
    groups = []
    while set().union(*groups) < set(graph.keys()):
        v = next(iter(set(graph.keys() - set().union(*groups))))
        g = group(graph, v)
        groups.append(g)
        if not v:
            print('Part 1 Solution: %d' % len(g))
    print('Part 2 Solution: %d' % len(groups))


print('Advent of Code 2017 - Day 12')
solve(sys.argv[1])
