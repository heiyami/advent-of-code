import sys


def caught(ranges, delay):
    """
    Returns True if caught while attempting to pass through the firewall
        with a given delay
    """
    curr = 0
    while curr < len(ranges):
        tick = curr + delay
        rnge = ranges[curr]
        loop = 2*(rnge-1)
        if not rnge:
            curr += 1
            continue
        if not tick % loop:
            break
        curr += 1
    return curr < len(ranges)


def severity(ranges, delay):
    """
    Returns the severity incurred from attempting to pass through the
        firewall with a given delay
    """
    curr, sev = 0, 0
    while curr < len(ranges):
        tick = curr + delay
        rnge = ranges[curr]
        loop = 2*(rnge-1)
        if not rnge:
            curr += 1
            continue
        if not tick % loop:
            sev += curr * ranges[curr]
        curr += 1
    return sev


def parse(file):
    """
    ranges[layer] = range
    """
    i = 0
    ranges = []
    with open(file, 'r') as f:
        for line in f:
            line = [int(x) for x in line.replace(':', '').split()]
            while i < line[0]:
                ranges.append(0)
                i += 1
            ranges.append(line[1])
            i += 1
    return ranges


def solve(file):
    ranges = parse(file)
    delay = 1
    print('Part 1 Solution: %d' % severity(ranges, 0))
    while caught(ranges, delay):
        delay += 1
    print('Part 2 Solution: %d' % delay)


print('Advent of Code 2017 - Day 13')
solve(sys.argv[1])
