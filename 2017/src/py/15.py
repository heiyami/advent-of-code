import sys


def parse(file):
    with open(file, 'r') as f:
        data = [int(line.split()[-1]) for line in f]
    return data


def solve1(file):
    verdict = 0
    a, b = parse(file)
    for i in range(0, 40000000):
        a = (16807 * a) % 2147483647
        b = (48271 * b) % 2147483647
        verdict += 1 if a & 65535 == b & 65535 else 0
    print('Part 1 Solution: %d' % verdict)


def solve2(file):
    verdict = 0
    a, b = parse(file)
    for i in range(0, 5000000):
        while True:
            a = (16807 * a) % 2147483647
            if not a & 3:
                break
        while True:
            b = (48271 * b) % 2147483647
            if not b & 7:
                break
        verdict += 1 if a & 65535 == b & 65535 else 0
    print('Part 2 Solution: %d' % verdict)


print('Advent of Code 2017 - Day 15')
solve1(sys.argv[1])
solve2(sys.argv[1])
