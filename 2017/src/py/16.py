import sys


order = [chr(x) for x in range(97,113)]


def parse(file):
    with open(file, 'r') as f:
        return [s for s in f.read().strip().split(',')]


def stringify(char_list):
    return ''.join(char_list)


def spin(x):
    tmp = order[-x:]
    order[x:] = order[:-x]
    order[:x] = tmp


def exchange(a, b):
    tmp = order[a]
    order[a] = order[b]
    order[b] = tmp


def partner(a, b):
    exchange(order.index(a), order.index(b))


def perform(move):
    m = move[0]
    e = move[1:]
    if m == 's':
        spin(int(e))
    elif m == 'x':
        exchange(int(e.split('/')[0]), int(e.split('/')[1]))
    elif m == 'p':
        partner(e.split('/')[0], e.split('/')[1])
    else:
        raise ValueError('Invalid move: %s' % move)


def solve(file):
    seen = []
    dance = parse(file)
    while True:
        for move in dance:
            perform(move)
        if stringify(order) in seen:
            break
        seen.append(stringify(order))
    n = 1000000000 % (len(seen))
    print(len(seen))
    print('Part 1 Solution: %s' % ''.join(seen[0]))
    print('Part 2 Solution: %s' % ''.join(seen[n-1]))


print('Advent of Code 2017 - Day 16')
solve(sys.argv[1])
