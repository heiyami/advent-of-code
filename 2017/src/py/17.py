class Node():
    def __init__(self, data, prev, next):
        self.data = data
        self.prev = prev
        self.next = next


class CircularBuffer():
    def __init__(self):
        root = Node(0, None, None)
        self.head = self.tail = root


    def insert(self, index, data):
        """Right-insert at index"""
        curr = self.head
        if not curr.next:
            node = Node(data, curr, curr)
            curr.prev = curr.next = node
        else:
            for i in range(index):
                curr = curr.next
            node = Node(data, curr, curr.next)
            curr.next.prev = node
            curr.next = node


    def get(self, data):
        """Return node with matching data"""
        curr = self.head
        while curr.data != data:
            curr = curr.next
        return curr


def solve1(steps, loops):
    buf = CircularBuffer()
    pos_i, pos_f = 0, 0
    for i in range(1, loops+1):
        pos_i = (pos_f + steps) % i
        pos_f = (pos_i + 1) % (i+1)
        buf.insert(pos_i, i)
    return buf.get(loops).next.data


def solve2(steps, loops):
    pos_i, pos_f = 0, 0
    for i in range(1, loops+1):
        pos_i = (pos_f + steps) % i
        pos_f = (pos_i + 1) % (i+1)
        if pos_f == 1:
            val = i
    return val


print('Advent of Code 2017 - Day 17')
print('Part 1 Solution: %d' % solve1(371, 2017))
print('Part 2 Solution: %d' % solve2(371, 50000000))
