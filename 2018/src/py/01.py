import sys


def part_one(input_list):
    sol = 0

    for delta in input_list:
        sol += int(delta)
    
    return sol


def part_two(input_list):
    sol = None

    freq = 0
    seen = set([0])
    freq_deltas = [int(delta) for delta in input_list]

    while sol is None:
        for delta in freq_deltas:
            freq += delta
            if freq in seen:
                sol = freq
                break
            else:
                seen.add(freq)

    return sol


def main():
    input_file = sys.argv[1]

    with open(input_file, 'r') as f:
        input_list = f.read().splitlines()

    print(f'Solution 1: {part_one(input_list)}')
    print(f'Solution 2: {part_two(input_list)}')


if __name__ == '__main__':
    main()
