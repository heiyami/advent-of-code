import sys


def get_char_count_map(id):
    """
    Returns a dict in which the keys are characters that appear in id
    and the values are the counts of the keys
    """
    char_count_map = {}

    for char in id:
        if char not in char_count_map:
            char_count_map[char] = 0
        char_count_map[char] += 1
    
    return char_count_map


def exists_char_of_count(id, n):
    """
    Returns True if there exists at least one char which appears n times in id
    """
    char_count_map = get_char_count_map(id)
    return any([count == n for count in char_count_map.values()])


def get_checksum(input_list):
    count_2, count_3 = 0, 0

    for id in input_list:
        if exists_char_of_count(id, 2):
            count_2 += 1
        if exists_char_of_count(id, 3):
            count_3 += 1
    
    checksum = count_2 * count_3
    return checksum


def part_one(input_list):
    sol = get_checksum(input_list)
    return sol


def replace_char_by_index(string, i, new_char):
    """
    Replaces the ith char of string with new_char
    """
    return string[:i] + new_char + string[i+1:]


def get_char_exclusion_dict(input_list):
    """
    Create a dict in which the keys are the original id in input_list,
    but with one char iteratively removed, and the values are the original id

    e.g. For the id 'abc', we create keys '_bc', 'a_c', 'ab_'
         and append 'abc' to the values of each
    """
    char_exclusion_dict = {}

    for id in input_list:
        for i, _ in enumerate(id):
            key = replace_char_by_index(id, i, '_')
            if key not in char_exclusion_dict:
                char_exclusion_dict[key] = set()
            char_exclusion_dict[key].add(id)
    
    return char_exclusion_dict


def part_two(input_list):
    ced = get_char_exclusion_dict(input_list)

    for k, v in ced.items():
        if len(v) == 2:
            sol = k.replace('_', '')
    
    return sol


def main():
    input_file = sys.argv[1]

    with open(input_file, 'r') as f:
        input_list = f.read().splitlines()

    print(f'Solution 1: {part_one(input_list)}')
    print(f'Solution 2: {part_two(input_list)}')


if __name__ == '__main__':
    main()
