import sys
import re


class SetMatrix:

    def __init__(self, length, width):
        self._length = length
        self._width = width
        self._matrix = [[set() for x in range(length)] for y in range(width)]
    
    def add_to_cell(self, x, y, value):
        self._matrix[y][x].add(value)

    def get_cell(self, x, y):
        return self._matrix[y][x]
    
    @property
    def length(self):
        return self._length
    
    @property
    def width(self):
        return self._width


class Claim:

    def __init__(self, id, dx, dy, length, width):
        self._id = id
        self._dx = dx
        self._dy = dy
        self._length = length
        self._width = width
    
    def __eq__(self, other):
        return self.id == other.id
    
    def fill_set_matrix(self, set_matrix):
        try:
            for y in range(self.width):
                for x in range(self.length):
                    set_matrix.add_to_cell(self.dx + x, self.dy + y, self.id)
        except IndexError:
            print('Claim exceeds dimensions of matrix!')
    
    @property
    def id(self):
        return self._id

    @property
    def dx(self):
        return self._dx

    @property
    def dy(self):
        return self._dy

    @property
    def length(self):
        return self._length

    @property
    def width(self):
        return self._width


def part_one(set_matrix):
    sol = 0

    for y in range(set_matrix.width):
        for x in range(set_matrix.length):
            if len(set_matrix.get_cell(x, y)) > 1:
                sol += 1
    
    return sol


def part_two(input_list, set_matrix):
    sol = None
    disjoint_claim_ids = set([claim.id for claim in input_list])

    # iterate through matrix, scanning for cells used in more than 1 claim
    # and remove such conflicting claims from disjoint_claim_ids
    for y in range(set_matrix.width):
        for x in range(set_matrix.length):
            curr = set_matrix.get_cell(x, y)
            if len(curr) > 1:
                for claim_id in curr:
                    if claim_id in disjoint_claim_ids:
                        disjoint_claim_ids.remove(claim_id)
    
    sol = disjoint_claim_ids.pop()
    return sol


def main():
    input_file = sys.argv[1]
    input_list = []

    with open(input_file, 'r') as f:
        for line in f:
            id, dx, dy, length, width = map(int, re.findall('\d+', line.strip()))
            curr = Claim(id, dx, dy, length, width)
            input_list.append(curr)
    
    set_matrix = SetMatrix(1000, 1000)

    for claim in input_list:
        claim.fill_set_matrix(set_matrix)


    print(f'Solution 1: {part_one(set_matrix)}')
    print(f'Solution 2: {part_two(input_list, set_matrix)}')


if __name__ == '__main__':
    main()
