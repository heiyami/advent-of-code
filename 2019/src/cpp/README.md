# Advent of Code 2019
This is a collection of solutions to problems in Advent of Code 2019.

## C++

### Compiling

Compile a specific solution for day N, where N = 01 to 25:
```
make day d=N
```

Compile all solutions available:
```
make all
```

Clean up:
```
make clean d=N
make clean-all
```

### Running

To run the solutions:

```
compile/<puzzle-solution> <puzzle-input-file>
```

For example, if the puzzle inputs are stored under `prompts/`:

```
compile/01 prompts/01
```

Be careful not to leave excess whitespace in the puzzle input files.

## Rust

:)

