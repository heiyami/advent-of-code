// File: 01.cpp
// Description: Solution to the puzzle of Advent of Code 2019, Day 01.

#include <iostream>
#include <cmath>

#include <string>
#include <vector>

#include "FileReader.cpp"


int calcFuelFromMassNaive(int mass)
{
    return std::floor(mass / 3) - 2;
}


int calcFuelFromMassAstute(int mass)
{
    int totalFuel = 0;

    int currMass = mass;
    int currFuel = 0;

    while (true)
    {
        currFuel = std::floor(currMass / 3) - 2;

        if (0 >= currFuel)
        {
            break;
        }

        totalFuel += currFuel;
        currMass = currFuel;
    }

    return totalFuel;
}


void PartOne(const std::vector<int>& masses)
{
    int totalFuel = 0;

    for (int i = 0; i < masses.size(); ++i)
    {
        int currFuel = calcFuelFromMassNaive(masses[i]);
        totalFuel += currFuel;
    }

    std::cout << "Part 1:" << std::endl;
    std::cout << "Total fuel required is " << totalFuel << " units." << std::endl;
}


void PartTwo(const std::vector<int>& masses)
{
    int totalFuel = 0;

    for (int i = 0; i < masses.size(); ++i)
    {
        int currFuel = calcFuelFromMassAstute(masses[i]);
        totalFuel += currFuel;
    }

    std::cout << "Part 2:" << std::endl;
    std::cout << "Total fuel required is " << totalFuel << " units." << std::endl;
}


int main(int argc, char* argv[])
{
    const std::string fp(argv[1]);

    try
    {
        AOCUtil::FileReader fr(fp);
        std::vector<std::string> lines;
        fr.ReadLines(lines);

        std::vector<int> masses;

        for (int i = 0; i < lines.size(); ++i)
        {
            masses.push_back(std::stoi(lines[i]));
        }

        PartOne(masses);
        PartTwo(masses);
    }
    catch (AOCUtil::Exception& e)
    {
        exit(2);
    }

    return 0;
}
