// File: 02.cpp
// Description: Solution to the puzzle of Advent of Code 2019, Day 02.

#include <iostream>
#include <cmath>

#include "IntcodeComputer.cpp"
#include "FileReader.cpp"


int RunGravityAssistProgram
(
    AOCUtil::IntcodeComputer& in_computer,
    std::vector<int>& in_values,
    int noun,
    int verb
)
{
    // Use input parameters noun and verb.
    in_values[1] = noun;
    in_values[2] = verb;

    // Run program.
    in_computer.Load(in_values);
    int result = in_computer.Run();

    return result;
}


void PartOne(AOCUtil::IntcodeComputer& in_computer, std::vector<int> values)
{
    int result = RunGravityAssistProgram(in_computer, values, 12, 2);

    std::cout << "Part 1:" << std::endl;
    std::cout << "Solution: " << result << std::endl;
}


void PartTwo
(
    AOCUtil::IntcodeComputer& in_computer,
    std::vector<int> values,
    int target
)
{
    int result;

    // Doubly-nested loop to find noun,verb pair.
    for (int noun = 0; noun < 100; ++noun)
    {
        // std::cout << "Using noun = " << noun << ": " << std::endl;
        for (int verb = 0; verb < 100; ++verb)
        {
            result = RunGravityAssistProgram(in_computer, values, noun, verb);

            if (target == result)
            {
                std::cout << "Part 2:" << std::endl;
                std::cout << "Solution: " << 100 * noun + verb << std::endl;

                return;
            }
        }

        // std::cout << std::endl;
    }

    std::cout << "Part 2:" << std::endl;
    std::cout << "Could not find a solution!" << std::endl;
}


int main(int argc, char* argv[])
{
    const std::string fp(argv[1]);

    try
    {
        AOCUtil::FileReader fr(fp);

        std::vector<int> values;
        fr.ReadCSVLine(values);

        AOCUtil::IntcodeComputer computer;
        PartOne(computer, values);
        PartTwo(computer, values, 19690720);
    }
    catch (AOCUtil::Exception& e)
    {
        exit(2);
    }

    return 0;
}
