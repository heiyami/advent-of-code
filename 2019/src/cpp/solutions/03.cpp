// File: 03.cpp
// Description: Solution to the puzzle of Advent of Code 2019, Day 03.

#include <iostream>
#include <cmath>

#include <string>
#include <unordered_set>
#include <vector>

#include "FileReader.cpp"
#include "Geometry.cpp"


namespace Geometry = AOCUtil::Geometry;


void PartOne()
{
    std::cout << "Part 1:" << std::endl;
    std::cout << "Solution: " << std::endl;
}


void PartTwo()
{
    std::cout << "Part 2:" << std::endl;
    std::cout << "Solution: " << std::endl;
}


int main(int argc, char* argv[])
{
    const std::string fp(argv[1]);

    try
    {
        AOCUtil::FileReader fr(fp);
        std::vector<std::vector<std::string>> lines;
        fr.ReadCSVLines(lines);

        for (auto iter = lines.begin(); iter != lines.end(); ++iter)
        {
            std::vector<std::string> inner = *iter;

            for (auto it = inner.begin(); it != inner.end(); ++it)
            {
                std::cout << *it << ", ";
            }

            std::cout << std::endl;
        }
    }
    catch (AOCUtil::Exception& e)
    {
        exit(2);
    }

    return 0;
}
