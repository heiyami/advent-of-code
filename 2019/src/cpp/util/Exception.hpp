// File: Exception.hpp
// Description: Custom exceptions to throw.
#ifndef AOCUTIL_EXCEPTION_HPP
#define AOCUTIL_EXCEPTION_HPP

#include <iostream>

namespace AOCUtil
{
    enum class Exception
    {
        InvalidFileNameError,
        InvalidOpcodeError,
        InvalidTypeError,
        InvalidValueError,
        NoOpenFileError,
        SeekError,
        GetLineError,
    };
}

#endif
