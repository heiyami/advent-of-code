// File: FileReader.cpp
// Description: Implementation of the FileReader class.

#include "Exception.hpp"
#include "FileReader.hpp"


using namespace AOCUtil;

// PUBLIC =====================================================================
FileReader::FileReader()
{
    ; // Let compiler write implicit default constructor.
}

FileReader::FileReader(const std::string& filename)
{
    __buf;
    __ifs.open(filename);

    if (!__ifs.good())
    {
        std::cerr << "ERROR: AOCUtil::FileReader::FileReader()" << std::endl;
        std::cerr << "       Failed to open file: " << filename << std::endl;

        throw Exception::InvalidFileNameError;
    }
}

FileReader::~FileReader()
{
    ; // Let compiler write implicit destructor.
}

void FileReader::ReadLine(std::string& out_line)
{
    if (!__ifs.is_open())
    {
        std::cerr << "ERROR: AOCUtil::FileReader::ReadLine()" << std::endl;
        std::cerr << "       No file associated with __ifs." << std::endl;

        throw Exception::NoOpenFileError;
    }

    std::getline(__ifs, out_line);

    if (!__ifs.good())
    {
        std::cerr << "ERROR: AOCUtil::FileReader::ReadLine()" << std::endl;
        std::cerr << "       Failed to read line." << std::endl;

        throw Exception::GetLineError;
    }

    return;
}

int FileReader::ReadLine()
{
    if (!__ifs.is_open())
    {
        std::cerr << "ERROR: AOCUtil::FileReader::ReadLine()" << std::endl;
        std::cerr << "       No file associated with __ifs." << std::endl;

        throw Exception::NoOpenFileError;
    }

    std::getline(__ifs, __buf);

    if (!__ifs.good())
    {
        std::cerr << "ERROR: AOCUtil::FileReader::ReadLine()" << std::endl;
        std::cerr << "       Failed to read line." << std::endl;

        throw Exception::GetLineError;
    }

    __buf.clear();

    return std::stoi(__buf);
}

void FileReader::ReadCSVLine(std::vector<std::string>& out_csvLine)
{
    if (!__ifs.is_open())
    {
        std::cerr << "ERROR: AOCUtil::FileReader::ReadLine()" << std::endl;
        std::cerr << "       No file associated with __ifs." << std::endl;

        throw Exception::NoOpenFileError;
    }

    std::getline(__ifs, __buf);

    if (!__ifs.good())
    {
        std::cerr << "ERROR: AOCUtil::FileReader::ReadLine()" << std::endl;
        std::cerr << "       Failed to read line." << std::endl;

        throw Exception::GetLineError;
    }

    __split(__buf, ',', out_csvLine);

    __buf.clear();
}

void FileReader::ReadCSVLine(std::vector<int>& out_csvLine)
{
    if (!__ifs.is_open())
    {
        std::cerr << "ERROR: AOCUtil::FileReader::ReadLine()" << std::endl;
        std::cerr << "       No file associated with __ifs." << std::endl;

        throw Exception::NoOpenFileError;
    }

    std::getline(__ifs, __buf);

    if (!__ifs.good())
    {
        std::cerr << "ERROR: AOCUtil::FileReader::ReadLine()" << std::endl;
        std::cerr << "       Failed to read line." << std::endl;

        throw Exception::GetLineError;
    }

    __split(__buf, ',', out_csvLine);

    __buf.clear();
}

void FileReader::ReadLines(std::vector<std::string>& out_lines)
{
    if (!__ifs.is_open())
    {
        std::cerr << "ERROR: AOCUtil::FileReader::ReadLine()" << std::endl;
        std::cerr << "       No file associated with __ifs." << std::endl;

        throw Exception::NoOpenFileError;
    }

    __ifs.seekg(0);

    if (!__ifs.good())
    {
        std::cerr << "ERROR: AOCUtil::FileReader::ReadLines()" << std::endl;
        std::cerr << "       Could not seek to offset." << std::endl;

        throw Exception::SeekError;
    }

    while (std::getline(__ifs, __buf))
    {
        if (!__ifs.good())
        {
            std::cerr << "ERROR: AOCUtil::FileReader::ReadLines()" << std::endl;
            std::cerr << "       Failed to read line." << std::endl;

            throw Exception::GetLineError;
        }

        out_lines.push_back(__buf);
    }

    __buf.clear();
}

void FileReader::ReadLines(std::vector<int>& out_lines)
{
    if (!__ifs.is_open())
    {
        std::cerr << "ERROR: AOCUtil::FileReader::ReadLine()" << std::endl;
        std::cerr << "       No file associated with __ifs." << std::endl;

        throw Exception::NoOpenFileError;
    }

    __ifs.seekg(0);

    if (!__ifs.good())
    {
        std::cerr << "ERROR: AOCUtil::FileReader::ReadLines()" << std::endl;
        std::cerr << "       Could not seek to offset." << std::endl;

        throw Exception::SeekError;
    }

    while (std::getline(__ifs, __buf))
    {
        if (!__ifs.good())
        {
            std::cerr << "ERROR: AOCUtil::FileReader::ReadLines()" << std::endl;
            std::cerr << "       Failed to read line." << std::endl;

            throw Exception::GetLineError;
        }

        if (std::string::npos != __buf.find_first_not_of("0123456789"))
        {
            std::cerr << "ERROR: AOCUtil::FileReader::ReadLines()" << std::endl;
            std::cerr << "       Input line " << __buf << " is not an integer." << std::endl;

            throw Exception::InvalidTypeError;
        }

        out_lines.push_back(std::stoi(__buf));
    }

    __buf.clear();
}

void FileReader::ReadCSVLines(std::vector<std::vector<std::string>>& out_csvLines)
{
    if (!__ifs.is_open())
    {
        std::cerr << "ERROR: AOCUtil::FileReader::ReadLine()" << std::endl;
        std::cerr << "       No file associated with __ifs." << std::endl;

        throw Exception::NoOpenFileError;
    }

    __ifs.seekg(0);

    if (!__ifs.good())
    {
        std::cerr << "ERROR: AOCUtil::FileReader::ReadLines()" << std::endl;
        std::cerr << "       Could not seek to offset." << std::endl;

        throw Exception::SeekError;
    }

    while (std::getline(__ifs, __buf))
    {
        if (!__ifs.good())
        {
            std::cerr << "ERROR: AOCUtil::FileReader::ReadLines()" << std::endl;
            std::cerr << "       Failed to read line." << std::endl;

            throw Exception::GetLineError;
        }

        std::vector<std::string> csvLine;
        __split(__buf, ',', csvLine);
        out_csvLines.push_back(csvLine);
    }

    __buf.clear();
}

void FileReader::ReadCSVLines(std::vector<std::vector<int>>& out_csvLines)
{
    if (!__ifs.is_open())
    {
        std::cerr << "ERROR: AOCUtil::FileReader::ReadLine()" << std::endl;
        std::cerr << "       No file associated with __ifs." << std::endl;

        throw Exception::NoOpenFileError;
    }

    __ifs.seekg(0);

    if (!__ifs.good())
    {
        std::cerr << "ERROR: AOCUtil::FileReader::ReadLines()" << std::endl;
        std::cerr << "       Could not seek to offset." << std::endl;

        throw Exception::SeekError;
    }

    while (std::getline(__ifs, __buf))
    {
        if (!__ifs.good())
        {
            std::cerr << "ERROR: AOCUtil::FileReader::ReadLines()" << std::endl;
            std::cerr << "       Failed to read line." << std::endl;

            throw Exception::GetLineError;
        }

        if (std::string::npos != __buf.find_first_not_of("0123456789"))
        {
            std::cerr << "ERROR: AOCUtil::FileReader::ReadLines()" << std::endl;
            std::cerr << "       Input line " << __buf << " is not an integer." << std::endl;

            throw Exception::InvalidTypeError;
        }

        std::vector<int> csvLine;
        __split(__buf, ',', csvLine);
        out_csvLines.push_back(csvLine);
    }

    __buf.clear();
}
// ==================================================================== PUBLIC

// PRIVATE ===================================================================
void FileReader::__split(
    std::string& in_input,
    char in_delim,
    std::vector<std::string>& out_tokens)
{
    std::stringstream tokenStream(in_input);

    while (std::getline(tokenStream, __buf, in_delim))
    {
        out_tokens.push_back(__buf);
    }

    __buf.clear();
}

void FileReader::__split(
    std::string& in_input,
    char in_delim,
    std::vector<int>& out_tokens)
{
    std::stringstream tokenStream(in_input);

    while (std::getline(tokenStream, __buf, in_delim))
    {
        out_tokens.push_back(std::stoi(__buf));
    }

    __buf.clear();
}
// =================================================================== PRIVATE
