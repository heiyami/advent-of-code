// File: FileReader.hpp
// Description: A collection of useful methods for reading input from Advent of Code.
#ifndef AOCUTIL_FILEREADER_HPP
#define AOCUTIL_FILEREADER_HPP

#include <fstream>
#include <iostream>

#include <sstream>
#include <string>
#include <vector>


namespace AOCUtil
{
    class FileReader
    {
    public:

        // FIELDS =============================================================
        // ============================================================= FIELDS

        // CONSTRUCTORS AND DESTRUCTORS =======================================
        // Desc: Default constructor.
        FileReader();

        // Desc: Parameterized constructor
        //       Associates __ifs with the file of the given file name.
        FileReader(const std::string& filename);

        // Desc: Destructor.
        ~FileReader();
        // ======================================= CONSTRUCTORS AND DESTRUCTORS

        // METHODS ============================================================
        // Desc: Reads the current line into the string, out_line.
        void ReadLine(std::string& out_line);

        // Desc: Reads the current line and returns its value as an integer.
        int ReadLine();

        // Desc: Reads the current line of comma-separated values
        //       into the vector of strings, out_csvLine.
        void ReadCSVLine(std::vector<std::string>& out_csvLine);

        // Desc: Reads the current line of comma-separated values
        //       into the vector of integers, out_csvLine.
        void ReadCSVLine(std::vector<int>& out_csvLine);

        // Desc: Reads every line into the vector of strings, out_lines.
        void ReadLines(std::vector<std::string>& out_lines);

        // Desc: Reads every line into the vector of integers, out_lines.
        void ReadLines(std::vector<int>& out_lines);

        // Desc: Reads the current line of comma-separated values
        //       into the 2D vector of strings, out_csvLines.
        void ReadCSVLines(std::vector<std::vector<std::string>>& out_csvLines);

        // Desc: Reads the current line of comma-separated values
        //       into the 2D vector of integers, out_csvLines.
        void ReadCSVLines(std::vector<std::vector<int>>& out_csvLines);
        // ============================================================ METHODS

    private:

        // FIELDS =============================================================
        std::string __buf;

        std::ifstream __ifs;
        // ============================================================= FIELDS

        // METHODS ============================================================
        // Desc: Splits the input string in_input by the delimiter in_delim and 
        //       stores the result into the vector of strings, out_tokens.
        void __split(
            std::string& in_input,
            char in_delim,
            std::vector<std::string>& out_tokens);
        

        // Desc: Splits the input string in_input by the delimiter in_delim and 
        //       stores the result into the vector of integers, out_tokens.
        void __split(
            std::string& in_input,
            char in_delim,
            std::vector<int>& out_tokens);
        // ============================================================ METHODS
    };
}

#endif
