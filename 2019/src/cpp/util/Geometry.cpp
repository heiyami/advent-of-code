// File: Geometry.cpp
// Desc: A collection of various geometric data types and functions.

#include <algorithm>
#include <cmath>

#include <string>
#include <vector>


namespace AOCUtil
{
namespace Geometry
{
    // Desc: A point on a 2D Cartesian plane.
    struct Point
    {
        int x;
        int y;

        // Desc: Overloaded less-than comparison operator.
        bool operator<(const Point& in_refPoint)
        {
            return (x + y < in_refPoint.x + in_refPoint.y);
        }

        // Desc: Calculates the Manhattan distance between theinput points.
        // Returns: The Manhattan distance.
        static int ManhattanDistance(const Point& in_a, const Point& in_b)
        {
            return std::abs(in_a.x - in_b.x) + std::abs(in_a.y - in_b.y);
        }
    };


    // Desc: A line on a 2D Cartesian plane.
    struct Line
    {
        Point head;
        Point tail;

        // Desc: Parameterized constructor.
        // Params: .
        Line(const Point& in_head, const std::string& in_linestr)
        {
            head = in_head;
            tail = head;

            char direction = in_linestr[0];
            std::string dist = in_linestr.substr(1);

            if ('U' == direction)
            {
                tail.y += std::stoi(dist);
            }
            if ('D' == direction)
            {
                tail.y -= std::stoi(dist);
            }
            if ('L' == direction)
            {
                tail.x -= std::stoi(dist);
            }
            if ('R' == direction)
            {
                tail.x += std::stoi(dist);
            }
        }

        // Desc: Returns a vector containing all Points along the line.
        void GetAllPoints(std::vector<Point>& io_points)
        {
            if (head.x == tail.x)
            {
                int min_y = std::min(head.y, tail.y);
                int max_y = std::max(head.y, tail.y);

                for (int i = min_y; i <= max_y; ++i)
                {
                    Point curr;
                    curr.x = head.x;
                    curr.y = i;
                    io_points.push_back(curr);
                }
            }
        }
    };


    // Desc: A wire on a 2D Cartesian plane. Consists of a union of lines.
    struct Wire
    {
        Point head;
        Point tail;
        std::vector<Line> lines;

        // Desc: Returns a vector containing all Points along the wire.
        void GetAllPoints(std::vector<Point>& io_points)
        {
            for (int i = 0; i < lines.size(); ++i)
            {
                std::vector<Point> currSet;
                lines[i].GetAllPoints(currSet);

                std::merge
                (
                    io_points.begin(), io_points.end(),
                    currSet.begin(), currSet.end(),
                    io_points.begin()
                );
            }
        }
    };
}
}

