// File: IComputer.hpp
// Desc: Definition of the IComputer interface.
#ifndef AOCUTIL_ICOMPUTER_HPP
#define AOCUTIL_ICOMPUTER_HPP

#include <array>
#include <memory>
#include <string>
#include <vector>

#include "Exception.hpp"


namespace AOCUtil
{
    typedef std::vector<int> Program;

    // Desc: A computer interface, abstracting program execution.
    class IComputer
    {
    public:
        // METHODS ============================================================
        // Desc: Load the program.
        virtual void Load(const Program& in_prog) = 0;

        // Desc: Save the program.
        virtual void Save(Program& io_prog) = 0;

        // Desc: Run the program loaded into __prog.
        // Returns: The result of the program.
        virtual int Run() = 0;
        // ============================================================ METHODS
    
    protected:

        // FIELDS =============================================================
        // Desc: The Intcode program. Also the computer's internal memory.
        std::vector<int> __prog;

        // Desc: Represents the current instruction.
        std::array<int, 8> __op;

        // Desc: The computer's internal program counter.
        int __pc = 0;
        // ============================================================= FIELDS

        // METHODS ============================================================

        // Desc: Fetches the current instruction pointed to by m_PC.
        virtual void __fetch() = 0;

        // Desc: Decodes the current instruction.
        virtual void __decode() = 0;

        // Desc: Executes the current instruction.
        // Returns: An integer value indicating how many instructions by which
        //          the program counter should increment.
        virtual int __execute() = 0;

        // Desc: Run one instruction.
        // Returns: A boolean value indicating whether to continue the program.
        virtual bool __tick() = 0;

        // Desc: Clears all internal storage.
        virtual void __clear() = 0;
        // ============================================================ METHODS
    };
}

#endif
