// File: IntCode.cpp
// Desc: Incode instruction set and implementation.


namespace AOCUtil
{
    namespace Intcode
    {
        // Desc: Reads the values from memory at positions a and b, then
        //       writes the sum a+b to the address pointed to by c.
        int ADD(Program& io_prog, int pc);

        // Desc: Reads the values from memory at positions a and b, then
        //       writes the product a*b to the address pointed to by c.
        int MUL(Program& io_prog, int pc);

        // Desc: No operation.
        int NOP();
    }

    int Intcode::ADD(Program& io_prog, int pc)
    {
        int i = io_prog[pc+1];
        int j = io_prog[pc+2];
        int k = io_prog[pc+3];

        io_prog[k] = io_prog[i] + io_prog[j];

        return 4;
    }

    int Intcode::MUL(Program& io_prog, int pc)
    {
        int i = io_prog[pc+1];
        int j = io_prog[pc+2];
        int k = io_prog[pc+3];

        io_prog[k] = io_prog[i] * io_prog[j];

        return 4;
    }

    int Intcode::NOP()
    {
        return 0;
    }
}
