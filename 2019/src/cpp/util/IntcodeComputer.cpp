// File: IntcodeComputer.cpp
// Desc: Implementation of the IComputer class with the Intcode instruction set.

#include "IComputer.hpp"
#include "Intcode.cpp"


namespace AOCUtil
{
    class IntcodeComputer : public IComputer
    {
    public:
        // FIELDS =============================================================
        // ============================================================= FIELDS

        // METHODS ============================================================
        virtual void Load(const Program& in_prog)
        {
            __prog = in_prog;
        }

        virtual void Save(Program& io_prog)
        {
            io_prog = __prog;
        }

        virtual int Run()
        {
            bool running = true;

            while (running)
            {
                running = __tick();
            }

            __clear();

            return __prog[0];
        }
        // ============================================================ METHODS
    private:
        // METHODS ============================================================
        virtual void __fetch()
        {
            __op[0] = __prog[__pc];
        }

        virtual void __decode()
        {
            switch (__op[0])
            {
                case 1:
                    __op[1] = __prog[__pc+1];
                    __op[2] = __prog[__pc+2];
                    __op[3] = __prog[__pc+3];
                    break;
                case 2:
                    __op[1] = __prog[__pc+1];
                    __op[2] = __prog[__pc+2];
                    __op[3] = __prog[__pc+3];
                    break;
                case 99:
                    break;
                default:
                    std::cerr << "ERROR: AOCUtil::IntcodeComputer::__decode()" << std::endl;
                    std::cerr << "       Bad opcode: " << __op[0] << std::endl;

                    throw Exception::InvalidOpcodeError;
            }
        }

        virtual int __execute()
        {
            switch (__op[0])
            {
                case 1:
                    return Intcode::ADD(__prog, __pc);
                case 2:
                    return Intcode::MUL(__prog, __pc);
                case 99:
                    return Intcode::NOP();
            }
        }

        virtual bool __tick()
        {
            __fetch();
            __decode();
            int inc = __execute();
            __pc += inc;

            return 0 != inc;
        }

        virtual void __clear()
        {
            __pc = 0;

            for (int i = 0; i < __op.size(); ++i)
            {
                __op[i] = 0;
            }
        }
        // ============================================================ METHODS
    };
}
