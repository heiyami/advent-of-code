import sys

def sol_1(nums):
    diffs = { 2020 - n for n in nums }
    for n in nums:
        if n in diffs:
            print(f'Solution 1: {n * (2020-n)}')
            return


def sol_2(nums):
    diffs = { 2020 - n for n in nums }
    for i in nums:
        for j in nums - { i }:
            if i+j in diffs:
                print(f'Solution 2: {i * j * (2020 - i - j)}')
                return


def main():
    input_file = sys.argv[1]

    with open(input_file, 'r') as f:
        input_list = f.read().splitlines()
        nums = { int(n) for n in input_list }

    sol_1(nums)
    sol_2(nums)


if __name__ == '__main__':
    main()
