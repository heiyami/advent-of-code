import sys

def sol_1(inputs):
    total = 0

    for inp in inputs:
        low = inp[0]
        high = inp[1]
        letter = inp[2]
        pw = inp[3]
        freq = pw.count(letter)

        if low <= freq and freq <= high:
            total += 1

    print(f'Solution 1: {total}')


def sol_2(inputs):
    total = 0

    for inp in inputs:
        p = inp[0] - 1
        q = inp[1] - 1
        letter = inp[2]
        pw = inp[3]

        if (pw[p] == letter) ^ (pw[q] == letter):
            total += 1

    print(f'Solution 2: {total}')


def main():
    input_file = sys.argv[1]

    with open(input_file, 'r') as f:
        input_list = f.read().splitlines()
        inputs = []
        for line in input_list:
            i = line.find('-')
            j = line.find(' ')
            k = line.find(':')

            m = int(line[:i])
            n = int(line[i+1:j])
            letter = line[j+1]
            pw = line[k+2:]
            inputs.append((m, n, letter, pw))

    sol_1(inputs)
    sol_2(inputs)


if __name__ == '__main__':
    main()
