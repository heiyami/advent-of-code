import sys

class AreaMap:

    __open_symbol = '.'
    __tree_symbol = '#'


    def __init__(self, in_map):
        self.__map = in_map
        self.len_x = len(in_map[0])
        self.len_y = len(in_map)


    def get_coord(self, x, y):
        mod_x = x % self.len_x
        return self.__map[y][mod_x]


    def is_open(self, x, y):
        return self.get_coord(x, y) == self.__open_symbol


    def is_tree(self, x, y):
        return self.get_coord(x, y) == self.__tree_symbol


def count_trees(area_map, init_x, init_y, dx, dy):
    total = 0
    curr_x = init_x
    curr_y = init_y

    while (curr_y < area_map.len_y):
        total += 1 if area_map.is_tree(curr_x, curr_y) else 0
        curr_x += dx
        curr_y += dy

    return total


def sol_1(area_map):
    total = count_trees(area_map, 0, 0, 3, 1)
    print(f'Solution 1: {total} trees were found.')


def sol_2(area_map):
    total = count_trees(area_map, 0, 0, 1, 1)
    total *= count_trees(area_map, 0, 0, 3, 1)
    total *= count_trees(area_map, 0, 0, 5, 1)
    total *= count_trees(area_map, 0, 0, 7, 1)
    total *= count_trees(area_map, 0, 0, 1, 2)
    print(f'Solution 2: Product of all trees found is {total}.')


def main():
    input_file = sys.argv[1]

    with open(input_file, 'r') as f:
        area_map = AreaMap(f.read().splitlines())

    sol_1(area_map)
    sol_2(area_map)


if __name__ == '__main__':
    main()
