import sys

from string import digits, hexdigits

class Passport:

    def __init__(self, passport_str):
        self.BIRTH_YEAR_KEY      = 'byr'
        self.ISSUE_YEAR_KEY      = 'iyr'
        self.EXPIRATION_YEAR_KEY = 'eyr'
        self.HEIGHT_KEY          = 'hgt'
        self.HAIR_COLOR_KEY      = 'hcl'
        self.EYE_COLOR_KEY       = 'ecl'
        self.PASSPORT_ID_KEY     = 'pid'
        self.COUNTRY_ID_KEY      = 'cid'

        self.__valid_ecls = set(['amb', 'blu', 'brn', 'gry', 'grn', 'hzl', 'oth'])

        self.__valid_fields = {
            self.BIRTH_YEAR_KEY:      self.__is_valid_byr,
            self.ISSUE_YEAR_KEY:      self.__is_valid_iyr,
            self.EXPIRATION_YEAR_KEY: self.__is_valid_eyr,
            self.HEIGHT_KEY:          self.__is_valid_hgt,
            self.HAIR_COLOR_KEY:      self.__is_valid_hcl,
            self.EYE_COLOR_KEY:       self.__is_valid_ecl,
            self.PASSPORT_ID_KEY:     self.__is_valid_pid,
            self.COUNTRY_ID_KEY:      self.__is_valid_cid
        }

        self.__fields = dict()

        for field in passport_str.split():
            key, val = field.split(':')
            if key in self.__valid_fields:
                self.__fields[key] = val


    def __len__(self):
        return len(self.__fields)


    def is_valid_one(self):
        has_all_fields = len(self.__fields) == len(self.__valid_fields)
        is_missing_one_field = len(self.__fields) == len(self.__valid_fields) - 1
        has_cid = self.COUNTRY_ID_KEY in self.__fields
        return has_all_fields or (is_missing_one_field and not has_cid)


    def is_valid_two(self):
        return self.is_valid_one() \
            and all([self.__is_valid_field(k,v) for k,v in self.__fields.items()])


    def __is_valid_field(self, key, val):
        return key in self.__valid_fields and self.__check_validity(key, val)


    def __check_validity(self, key, val):
        return self.__valid_fields[key](val)


    def __is_valid_byr(self, byr):
        try:
            return len(byr) == 4 and 1920 <= int(byr) and int(byr) <= 2002
        except ValueError:
            return False


    def __is_valid_iyr(self, iyr):
        try:
            return len(iyr) == 4 and 2010 <= int(iyr) and int(iyr) <= 2020
        except ValueError:
            return False


    def __is_valid_eyr(self, eyr):
        try:
            return len(eyr) == 4 and 2020 <= int(eyr) and int(eyr) <= 2030
        except ValueError:
            return False


    def __is_valid_hgt(self, hgt):
        height_val = int(hgt.replace('cm','').replace('in',''))
        if hgt[-2:] == 'cm':
            return 150 <= height_val and height_val <= 193
        elif hgt[-2:] == 'in':
            return 59 <= height_val and height_val <= 76
        else:
            return False


    def __is_valid_hcl(self, hcl):
        return len(hcl) == 7 and hcl[0] == '#' \
            and all([hcl[i] in hexdigits for i in range(1, len(hcl))])


    def __is_valid_ecl(self, ecl):
        return ecl in self.__valid_ecls


    def __is_valid_pid(self, pid):
        return len(pid) == 9 and all(c in digits for c in pid)


    def __is_valid_cid(self, cid):
        return True


def sol_1(passports):
    total = 0

    for pp in passports:
        total += 1 if pp.is_valid_one() else 0

    print(f'Solution 1: {total}')


def sol_2(passports):
    total = 0

    for pp in passports:
        total += 1 if pp.is_valid_two() else 0

    print(f'Solution 2: {total}')


def main():
    input_file = sys.argv[1]

    with open(input_file, 'r') as f:
        passports = [ Passport(line) for line in f.read().split('\n\n') ]

    sol_1(passports)
    sol_2(passports)


if __name__ == '__main__':
    main()
