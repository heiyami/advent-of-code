import sys

def parse_seat(seat):
    encoded_row = seat[:7]
    encoded_col = seat[-3:]

    row_head = 0
    row_tail = 127
    col_head = 0
    col_tail = 7

    for c in encoded_row:
        if c == 'F':
            row_tail -= (row_tail - row_head + 1) // 2
        elif c == 'B':
            row_head += (row_tail - row_head + 1) // 2
        else:
            raise ValueError(f'Invalid row sequence: {encoded_row}')

    for c in encoded_col:
        if c == 'L':
            col_tail -= (col_tail - col_head + 1) // 2
        elif c == 'R':
            col_head += (col_tail - col_head + 1) // 2
        else:
            raise ValueError(f'Invalid column sequence: {encoded_col}')

    assert(row_head == row_tail)
    assert(col_head == col_tail)

    return row_head, col_head


def calc_id(row, col):
    return 8 * row + col


def sol_1(seats):
    id_set = set()

    for seat in seats:
        row, col = parse_seat(seat)
        id_set.add(calc_id(row, col))

    print(f'Solution 1: {max(id_set)}')


def sol_2(seats):
    seating_matrix = [[0] * 8 for i in range(128)]

    for seat in seats:
        row, col = parse_seat(seat)
        seating_matrix[row][col] = 1

    for row in range(len(seating_matrix)):
        if sum(seating_matrix[row]) == 7:
            col = seating_matrix[row].index(0)
            print(f'Solution 2: {calc_id(row, col)}')
            return


def main():
    input_file = sys.argv[1]

    with open(input_file, 'r') as f:
        seats = f.read().splitlines()

    sol_1(seats)
    sol_2(seats)


if __name__ == '__main__':
    main()
