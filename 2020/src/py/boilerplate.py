import sys

def sol_1(inputs):
    pass


def sol_2(inputs):
    pass


def main():
    input_file = sys.argv[1]

    with open(input_file, 'r') as f:
        pass

    sol_1(inputs)
    sol_2(inputs)


if __name__ == '__main__':
    main()
