use std::collections::VecDeque;
use std::io::Error;

use crate::util::file::read_lines_as_vec;

pub fn run() -> Result<(String, String), Error> {
    let input = read_lines_as_vec::<usize>("input/01")?;
    Ok((part_one(&input).to_string(), part_two(&input).to_string()))
}

fn part_one(input: &Vec<usize>) -> usize {
    let mut solution: usize = 0;
    let mut prev_depth: usize = 0;

    for curr_depth in input {
        if *curr_depth > prev_depth {
            solution += 1;
        }

        prev_depth = *curr_depth;
    }

    solution - 1
}

fn part_two(input: &Vec<usize>) -> usize {
    let mut solution: usize = 0;
    let mut prev_window_sum: usize = 0;
    let mut depth_cache: VecDeque<usize> = VecDeque::with_capacity( 2);

    for curr_depth in input {
        if depth_cache.len() == 2 {
            let curr_window_sum = curr_depth
                + depth_cache.front().unwrap()
                + depth_cache.back().unwrap();

            if curr_window_sum > prev_window_sum {
                solution += 1;
            }

            prev_window_sum = curr_window_sum;
            depth_cache.pop_front();
        }
        depth_cache.push_back(*curr_depth);
    }

    solution - 1
}

