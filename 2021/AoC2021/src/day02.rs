use std::io::Error;

use crate::util::file::read_lines_as_vec;

pub fn run() -> Result<(String, String), Error> {
    let input = read_lines_as_vec::<String>("input/02")?;
    Ok((part_one(&input), part_two(&input)))
}

fn part_one(input: &Vec<(String)>) -> String {
    let mut x = 0;
    let mut y = 0;

    for line in input {
        let mut split = line.split(' ');
        let direction = split.next().unwrap();
        let units = split.next().unwrap().parse::<i32>().unwrap();

        if direction == "forward" {
            x += units;
        } else if direction == "up" {
            y -= units;
        } else if direction == "down" {
            y += units;
        }
    }

    (x*y).to_string()
}

fn part_two(input: &Vec<(String)>) -> String {
    let mut x = 0;
    let mut y = 0;
    let mut aim = 0;

    for line in input {
        let mut split = line.split(' ');
        let direction = split.next().unwrap();
        let units = split.next().unwrap().parse::<i64>().unwrap();

        if direction == "forward" {
            x += units;
            y += units * aim;
        } else if direction == "up" {
            aim -= units;
        } else if direction == "down" {
            aim += units;
        }
    }

    (x*y).to_string()
}
