use std::io::Error;
use std::process::exit;
use structopt::StructOpt;

use aoc2021;

#[derive(Debug, StructOpt)]
struct Opt {
    /// The day of the puzzle.
    day: usize,
}

fn run() -> Result<(), Error> {
    let opt = Opt::from_args();

    let (solution_one, solution_two) = match opt.day {
        1 => aoc2021::day01::run()?,
        2 => aoc2021::day02::run()?,
        n if n > 0 && n < 26 => panic!("Day {} is not yet implemented.", n),
        _ => panic!("Day must be between 1 and 25, inclusive."),
    };

    println!("{}", solution_one);
    println!("{}", solution_two);

    Ok(())
}

fn main() {
    if let Err(e) = run() {
        eprintln!("{}", e);
        let mut e: &dyn std::error::Error = &e;
        while let Some(source) = e.source() {
            eprintln!("  - caused by: {}", source);
            e = source;
        }
        exit(1);
    }
}
