use std::fs::File;
use std::io::{BufRead, BufReader, Error};
use std::str::FromStr;

pub mod file {
    use super::*;

    /// Opens the file given by the given file name and reads the lines into a vector of T.
    pub fn read_lines_as_vec<T>(filename: &str) -> Result<Vec<T>, Error>
    where
        T: FromStr,
        <T as FromStr>::Err: std::fmt::Debug,
    {
        let file = File::open(filename).unwrap();
        let reader = BufReader::new(file);
        let mut lines: Vec<T> = Vec::new();

        for line in reader.lines() {
            match line {
                Ok(s) => {
                    let elem = s.parse::<T>().unwrap();
                    lines.push(elem);
                },
                Err(why) => panic!("{:?}", why),
            }
        }

        Ok(lines)
    }
}